using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SettingsManager : MonoBehaviour
{

    public static SettingsManager instance;

    [SerializeField] Button startButton;
    [SerializeField] List<ButtonLogic> buttons;
    [SerializeField] List<Button> buttonsToDisable;
[   SerializeField] TMP_Text loading;

    private Color color = Color.white;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        buttons.ForEach(button =>
        {
            if (button != null)
            {
                button.OnButtonPressed += ButtonLogic_OnButtonPressed;
            }
        });
    }

    private void ButtonLogic_OnButtonPressed(object sender, ButtonLogic.OnButtonPressedEventArgs e)
    {
        color = e.color;
        startButton.image.color = color;
    }

    public Color GetColor()
    {
        return color;
    }

    public void OnStartPress()
    {
        loading.gameObject.SetActive(true);
        foreach (Button element in buttonsToDisable)
        {
            element.gameObject.SetActive(false);
        }

        Invoke(nameof(runLoadScrean), 2f);
    }

    public void runLoadScrean() {
        StartCoroutine(LoadYourAsyncScene());
    }

    public IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("GameScene");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}